<?php

namespace App\Http\Controllers;

use App\oportunidades;
use Illuminate\Http\Request;

class OportunidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $op = new oportunidades();
        $registros = $op->all();
        return json_encode($registros,JSON_PRETTY_PRINT);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $dados = json_encode($request->all(),true);
        $dados = json_decode($dados);
        $registros_inseridos = 0;

        if(isset($dados->oportunidades)){

            foreach($dados->oportunidades as $tupla){

                $lead = $tupla->lead;

                //Verifica se ficou algum campo em branco ou o mesmo não foi informado

                //Campo nome_identificador
                if(!isset($lead->nome_identificador) || ($lead->nome_identificador == '')){
                    return 'Não é possível inserir sem informar o campo "nome_identificador"';
                }

                //Campo tipo_contato
                if(!isset($lead->tipo_contato) || ($lead->tipo_contato == '')){
                    return 'Não é possível inserir sem informar o campo "tipo_contato"';
                }

                //Campo datalhe_contato
                if(!isset($lead->detalhe_contato) || ($lead->detalhe_contato == '')){
                    return 'Não é possível inserir sem informar o campo "detalhe_contato"';
                }

                //Campo status_atendimento
                if(!isset($lead->status_atendimento) || ($lead->status_atendimento == '')){
                    return 'Não é possível inserir sem informar o campo "status_atendimento"';
                }

                //Campo responsavel_atendimento
                if(!isset($lead->responsavel_atendimento) || ($lead->responsavel_atendimento == '')){
                    return 'Não é possível inserir sem informar o campo "responsavel_atendimento"';
                }

                //Campo veiculo_de_interesse
                if(!isset($lead->veiculo_de_interesse) || ($lead->veiculo_de_interesse == '')){
                    return 'Não é possível inserir sem informar o campo "veiculo_de_interesse"';
                }

                //Campo data
                if(!isset($lead->data) || ($lead->data == '')){
                    return 'Não é possível inserir sem informar o campo "data"';
                }  else {
                    if(preg_match("/\//",$lead->data)){
                        //Tratar a data para mysql
                        $data = explode('/',$lead->data);
                        $lead->data = $data[2].'-'.$data[1].'-'.$data[0];
                    }
                }                                             

                //Campo hora
                if(!isset($lead->hora) || ($lead->hora == '')){
                    return 'Não é possível inserir sem informar o campo "hora"';
                }        



                //Verificar duplicidade
                $op = new oportunidades(); 

                
                if (trim($op->where( #Se o registro já existir [nome_identificador,data,hora,tipo_contato] não permite o registro
                    [                    
                        'nome_identificador'=>$lead->nome_identificador,
                        'data'=>$lead->data,
                        'hora'=>$lead->hora,
                        'tipo_contato'=>$lead->tipo_contato
                    ]
                    )->get()) != '[]') 
                {
                
                    echo 'O Registro já existe
                    ';
                } else { #Caso o registro não exista, preenche cada parametro informado e salva os dados
                
                    //Inserir o lead tratado
                    $op = new oportunidades();  

                    $op->nome_identificador = $lead->nome_identificador;
                    $op->tipo_contato = $lead->tipo_contato;
                    $op->detalhe_contato = $lead->detalhe_contato;
                    $op->veiculo_de_interesse = $lead->veiculo_de_interesse;
                    $op->responsavel_atendimento = $lead->responsavel_atendimento;
                    $op->status_atendimento = $lead->status_atendimento;
                    $op->data = $lead->data;
                    $op->hora = $lead->hora;

                    $op->save();

                    $registros_inseridos++;

                }

                         
            }

            echo utf8_encode($registros_inseridos.'REGISTRO[S] ADICIONADOS COM SUCESSO!');

        } else {
            echo '<pre>';
            echo ' ESTE ENDPOINT ACEITA A INSERCAO DE UM OU MAIS REGISTROS EM JSON NO SEGUINTE FORMATO:
            {
                "oportunidades": [
                    {
                        "lead": {
                            "nome_identificador": "(31) 99787-5701",
                            "tipo_contato": "Liga\u00e7\u00e3o",
                            "detalhe_contato": "00:05:09",
                            "status_atendimento": "Atendida",
                            "responsavel_atendimento": "Encaminhar",
                            "veiculo_de_interesse": "Ve\u00edculo n\u00e3o informado",
                            "data": "20\/10\/2017",
                            "hora": "12:45"
                        }
                    }
                    {
                        "lead": {
                            ... []
                        }
                    }
                }
            }

            ';
            echo '</pre>';
        }

        

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\oportunidades  $oportunidades
     * @return \Illuminate\Http\Response
     */
    public function show($id,oportunidades $oportunidades)
    {
        return json_encode($oportunidades->find($id),JSON_PRETTY_PRINT);

        //return view('lead',['lead'=>$oportunidades->find($id)]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\oportunidades  $oportunidades
     * @return \Illuminate\Http\Response
     */
    public function edit(oportunidades $oportunidades)
    {
        //
        return 'A edição ainda não está funcional!';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\oportunidades  $oportunidades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, oportunidades $oportunidades)
    {
        //
        return 'Um registro de oportunidade não pode ser editado';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\oportunidades  $oportunidades
     * @return \Illuminate\Http\Response
     */
    public function destroy(oportunidades $oportunidades)
    {
        //
        return 'Não é possível excluir um registro de oportunidade';
    }
}
