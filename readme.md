# API de prospects

	Inserção listagem e visualização dos leads através dos segintes endpoints:
	
		Inserção de leads POST /api/leads/   
		Listagem de leads inseridos GET  /api/leads/
		Visualização de 1 lead específico /api/lead/{id}
		
	Implantação:
	
		rodar composer install para baixar as dependências
		criar/configurar arquivo /.env com as informações de acesso ao banco MYSQL (copiar o de exemplo e alterar)
		setar permissão 755 para ./artisan
		executar ./artisan key:generate para atualizar a chave de segurança do aplicativo
		executar ./artisan migrate para rodar as rotinas de banco de dados necessárias
		
		subir servidor com ./artisan serve
		
		




