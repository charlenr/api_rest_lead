<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>APIRESTfull - Lead Web Crawler</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #333;
                font-family: 'Raleway', sans-serif;
                font-weight: 400;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .blocos {
            	clear:both !important;
            }
        </style>
    </head>
    <body>
        <div class="flex-center">
        	{{ $lead->nome_identificador }}
        </div>
        <div class="flex-center">
        	{{ $lead->tipo_contato }}
        </div>
        <div class="flex-center">
        	{{ $lead->detalhe_contato }}
        </div>
        <div class="flex-center">
        	{{ $lead->status_atendimento }}
        </div>  
        <div class="flex-center">
        	{{ $lead->responsavel_atendimento }}
        </div>
        <div class="flex-center">
        	{{ $lead->veiculo_de_interesse }}
        </div>
        <div class="flex-center">
        	{{ dtBr($lead->data) }}
        </div>
        <div class="flex-center">
        	{{ $lead->hora }}
        </div>       
    </body>
</html>
<?php

function dtBr($data){
	$data = explode('-',$data);
	return $data[2].'/'.$data[1].'/'.$data[0];
}

?>